/* Dooba SDK
 * HTTP Client / Server
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include <util/cprintf.h>

// Internal Includes
#include "server.h"

// Create HTTP Server
uint8_t http_server_start(struct http_server *srv, uint16_t port, struct socket_iface *iface, http_server_req_handler_t req_handler, void *user)
{
	uint8_t i;

	// Configure Server
	srv->data_rxd = 0;
	srv->update_timer = 0;
	srv->req_handler = req_handler;
	srv->user = user;
	srv->ehook_on = 0;

	// Clear Clients
	for(i = 0; i < HTTP_SERVER_MAX_CLIENTS; i = i + 1)																{ srv->clients[i].conn.sck = 0; }

	// Create Server
	return socket_lstn(iface, &(srv->sck), port, srv, (socket_cnct_handler_t)http_server_on_cnct, (socket_recv_handler_t)http_server_on_recv, (socket_dcnt_handler_t)http_server_on_dcnt);
}

// Drop HTTP Server
void http_server_stop(struct http_server *srv)
{
	// Check Socket
	if(!(srv->sck))																									{ return; }

	// Unhook Eloop
	if(srv->ehook_on)																								{ eloop_unhook(&(srv->ehook)); }
	srv->ehook_on = 0;

	// Close Socket
	socket_close(srv->sck);
	srv->sck = 0;
}

// Daemonize (Run server in background)
void http_server_daemonize(struct http_server *srv)
{
	// Hook Eloop
	eloop_hook(&(srv->ehook), (eloop_hook_t)http_server_update, srv);
	srv->ehook_on = 1;
}

// Update HTTP Server
void http_server_update(struct http_server *srv)
{
	uint8_t i;
	struct http_server_client *c;

	// Check Update Timer (but still update if data received)
	srv->update_timer = srv->update_timer + 1;
	if((srv->update_timer < HTTP_SERVER_UPDATE_INTERVAL) && (srv->data_rxd == 0))									{ return; }
	srv->update_timer = 0;
	srv->data_rxd = 0;

	// Run through Clients
	for(i = 0; i < HTTP_SERVER_MAX_CLIENTS; i = i + 1)
	{
		// Acquire Client
		c = &(srv->clients[i]);

		// Check Client
		if(c->conn.sck)
		{
			// Update Timer
			c->timer = c->timer + 1;
			if(c->timer >= HTTP_SERVER_CLIENT_TIMEOUT)																{ http_server_flush(c); http_server_drop(c); }
			else
			{
				// Send Continue
				if(c->continue_sent == 2)
				{
					http_server_printf(c, "HTTP/1.1 %i %s\r\n\r\n", HTTP_RES_CONTINUE, HTTP_RESPONSE_CONTINUE);
					http_server_flush(c);
					c->continue_sent = 1;
				}

				// Check Request Complete
				if(c->req_complete)
				{
					// Handle Request
					srv->req_handler(srv->user, c);

					// Complete Response, Flush & Drop Client
					if(c->res_status == HTTP_SERVER_RES_STATUS_NONE)												{ http_server_respond(c, HTTP_RES_SRVERROR); }
					if(c->res_status != HTTP_SERVER_RES_STATUS_DONE)												{ http_server_resp_end(c); }
					http_server_flush(c);
					http_server_drop(c);
				}
			}
		}
	}
}

// Start Response
void http_server_respond(struct http_server_client *c, uint16_t code)
{
	// Check Response Status
	if(c->res_status != HTTP_SERVER_RES_STATUS_NONE)																{ return; }

	// Issue Response Code & Minimal Headers
	c->res_status = HTTP_SERVER_RES_STATUS_HEAD;
	http_server_printf(c, "HTTP/1.1 %i %s\r\n", code, http_code2resp(code));
	http_server_header(c, HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTIONTYPE_CLOSE);
}

// Complete Response
void http_server_resp_end(struct http_server_client *c)
{
	// Check Response Status
	if((c->res_status == HTTP_SERVER_RES_STATUS_NONE) || (c->res_status == HTTP_SERVER_RES_STATUS_DONE))			{ return; }

	// Complete Response
	if(c->res_status == HTTP_SERVER_RES_STATUS_HEAD)																{ http_server_printf(c, "\r\n"); }
	c->res_status = HTTP_SERVER_RES_STATUS_DONE;
	http_server_flush(c);
}

// Add Response Body
void http_server_resp_body(struct http_server_client *c, void *body)
{
	// Add Response Body
	http_server_resp_body_n(c, body, strlen(body));
}

// Add Response Body - Fixed Size
void http_server_resp_body_n(struct http_server_client *c, void *body, uint16_t body_len)
{
	// Check Response Status
	if(c->res_status != HTTP_SERVER_RES_STATUS_HEAD)																{ return; }

	// Send Content-Length Header & Terminate Header Section
	http_server_header(c, HTTP_HEADER_CONTENTLENGTH, "%i", body_len);
	http_server_printf(c, "\r\n");

	// Send Body
	c->res_status = HTTP_SERVER_RES_STATUS_BODY;
	http_server_printf(c, "%s", body, body_len);
}

// Redirect
void http_server_redirect(struct http_server_client *c, char *fmt, ...)
{
	va_list ap;

	// Check Response Status
	if(c->res_status != HTTP_SERVER_RES_STATUS_NONE)																{ return; }

	// Acquire Args
	va_start(ap, fmt);

	// Start Response & Set Location Header
	http_server_respond(c, HTTP_RES_FOUND);
	http_server_header_v(c, HTTP_HEADER_LOCATION, fmt, ap);

	// Release Args
	va_end(ap);
}

// Connect Handler (Socket Interface)
void http_server_on_cnct(struct http_server *srv, struct socket *s)
{
	uint8_t i;
	struct http_server_client *c;

	// Find Free Client
	i = 0;
	while((i < HTTP_SERVER_MAX_CLIENTS) && (srv->clients[i].conn.sck))												{ i = i + 1; }
	if(i >= HTTP_SERVER_MAX_CLIENTS)																				{ socket_close(s); return; }

	// Configure Client
	c = &(srv->clients[i]);
	s->srv_data = c;
	c->conn.sck = s;
	c->conn.ibuf = c->reqbuf;
	c->conn.obuf = c->resbuf;
	c->conn.obuf_p = 0;
	c->conn.obuf_size = HTTP_SERVER_RESBUF_SIZE;
	c->timer = 0;
	c->req_complete = 0;
	c->continue_sent = 0;
	c->res_status = HTTP_SERVER_RES_STATUS_NONE;
	c->reqbuf_p = 0;
}

// Data Handler (Socket Interface)
void http_server_on_recv(struct http_server *srv, struct socket *s, uint8_t *data, uint16_t size)
{
	struct http_server_client *c;
	struct http_arg *a;

	// Set Data Received
	srv->data_rxd = 1;

	// Acquire Client
	c = (struct http_server_client *)(s->srv_data);

	// Append Request Data to Buffer
	if((c->reqbuf_p + size) > HTTP_SERVER_REQBUF_SIZE)																{ size = HTTP_SERVER_REQBUF_SIZE - c->reqbuf_p; }
	memcpy(&(c->reqbuf[c->reqbuf_p]), data, size);
	c->reqbuf_p = c->reqbuf_p + size;

	// Attempt to parse HTTP Request
	if(http_split_req((char *)c->reqbuf, c->reqbuf_p, &(c->req)))													{ return; }

	// Check Host
	if(!(http_server_req_head(c, HTTP_HEADER_HOST)))																{ return; }

	// Check Expect
	a = http_server_req_head(c, HTTP_HEADER_EXPECT);
	if(a)
	{
		// Check / Send Continue
		if((c->continue_sent == 0) && (strncasecmp(http_arg_val(c, a), "100-continue", a->val_len) == 0))			{ c->continue_sent = 2; }
	}

	// Check Content-Length
	a = http_server_req_head(c, HTTP_HEADER_CONTENTLENGTH);
	if(a)																											{ if(c->req.body_len < atoi(http_arg_val(c, a))) { return; } }

	// Mark Request Complete and Ready for Processing
	c->req_complete = 1;
}

// Disconnect Handler (Socket Interface)
void http_server_on_dcnt(struct http_server *srv, struct socket *s)
{
	struct http_server_client *c;

	// Drop Client
	c = (struct http_server_client *)(s->srv_data);
	http_server_drop(c);
}
