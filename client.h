/* Dooba SDK
 * HTTP Client / Server
 */

#ifndef	__HTTP_CLIENT_H
#define	__HTTP_CLIENT_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <eloop/eloop.h>
#include <socket/iface.h>
#include <socket/socket.h>

// Internal Includes
#include "http.h"
#include "connector.h"

// Request Buffer Size
#ifndef	HTTP_CLIENT_REQBUF_SIZE
#define	HTTP_CLIENT_REQBUF_SIZE																256
#endif

// Request Form Buffer Size
#ifndef	HTTP_CLIENT_FORMBUF_SIZE
#define	HTTP_CLIENT_FORMBUF_SIZE															256
#endif

// Response Buffer Size
#ifndef	HTTP_CLIENT_RESBUF_SIZE
#define	HTTP_CLIENT_RESBUF_SIZE																512
#endif

// Request States
#define	HTTP_CLIENT_REQ_STATE_NONE															0x00
#define	HTTP_CLIENT_REQ_STATE_HEAD															0x01
#define	HTTP_CLIENT_REQ_STATE_FORM															0x02
#define	HTTP_CLIENT_REQ_STATE_BODY															0x03
#define	HTTP_CLIENT_REQ_STATE_RESP															0x04
#define	HTTP_CLIENT_REQ_STATE_DONE															0x05
#define	HTTP_CLIENT_REQ_STATE_FAIL															0x06

// Request Shortcut Parameters
#define	HTTP_CLIENT_HEADER																	0x01
#define	HTTP_CLIENT_FORMDATA																0x02

// Generic Var Args Flags
#define	HTTP_CLIENT_VARGS_HEADERS															0x01
#define	HTTP_CLIENT_VARGS_FORMDATA															0x02

// GET
#define	http_get(c, iface, url_fmt, ...)													http_client_req(c, iface, HTTP_METH_GET, 0, 0, url_fmt, ##__VA_ARGS__)
#define	http_get_v(c, iface, url_fmt, args)													http_client_req_v(c, iface, HTTP_METH_GET, 0, 0, url_fmt, args)

// POST
#define	http_post(c, iface, url_fmt, ...)													http_client_req(c, iface, HTTP_METH_POST, 0, 0, url_fmt, ##__VA_ARGS__)
#define	http_post_v(c, iface, url_fmt, args)												http_client_req_v(c, iface, HTTP_METH_POST, 0, 0, url_fmt, args)
#define	http_post_data(c, iface, data, size, url_fmt, ...)									http_client_req(c, iface, HTTP_METH_POST, data, size, url_fmt, ##__VA_ARGS__)
#define	http_post_data_v(c, iface, data, size, url_fmt, args)								http_client_req_v(c, iface, HTTP_METH_POST, data, size, url_fmt, args)

// PUT
#define	http_put(c, iface, url_fmt, ...)													http_client_req(c, iface, HTTP_METH_PUT, 0, 0, url_fmt, ##__VA_ARGS__)
#define	http_put_v(c, iface, url_fmt, args)													http_client_req_v(c, iface, HTTP_METH_PUT, 0, 0, url_fmt, args)
#define	http_put_data(c, iface, data, size, url_fmt, ...)									http_client_req(c, iface, HTTP_METH_PUT, data, size, url_fmt, ##__VA_ARGS__)
#define	http_put_data_v(c, iface, data, size, url_fmt, args)								http_client_req_v(c, iface, HTTP_METH_PUT, data, size, url_fmt, args)

// PATCH
#define	http_patch(c, iface, url_fmt, ...)													http_client_req(c, iface, HTTP_METH_PATCH, 0, 0, url_fmt, ##__VA_ARGS__)
#define	http_patch_v(c, iface, url_fmt, args)												http_client_req_v(c, iface, HTTP_METH_PATCH, 0, 0, url_fmt, args)
#define	http_patch_data(c, iface, data, size, url_fmt, ...)									http_client_req(c, iface, HTTP_METH_PATCH, data, size, url_fmt, ##__VA_ARGS__)
#define	http_patch_data_v(c, iface, data, size, url_fmt, args)								http_client_req_v(c, iface, HTTP_METH_PATCH, data, size, url_fmt, args)

// DELETE
#define	http_delete(c, iface, url_fmt, ...)													http_client_req(c, iface, HTTP_METH_DELETE, 0, 0, url_fmt, ##__VA_ARGS__)
#define	http_delete_v(c, iface, url_fmt, args)												http_client_req_v(c, iface, HTTP_METH_DELETE, 0, 0, url_fmt, args)

// Request Shortcuts
#define	http_client_failed(c)																((c)->state == HTTP_CLIENT_REQ_STATE_FAIL)

// Response Shortcuts
#define	http_client_res(c)																	(&((c)->res))
#define	http_client_res_ver(c)																((char *)((c)->resbuf))
#define	http_client_res_ver_l(c)															(http_client_res(c)->ver_len)
#define	http_client_res_status(c)															(http_client_res(c)->status)
#define	http_client_res_status_line(c)														((char *)(&((c)->resbuf[http_client_res(c)->status_off])))
#define	http_client_res_status_line_l(c)													(http_client_res(c)->status_len)
#define	http_client_res_status_txt(c)														((char *)(&((c)->resbuf[http_client_res(c)->status_txt_off])))
#define	http_client_res_status_txt_l(c)														(http_client_res(c)->status_txt_len)
#define	http_client_res_body(c)																((char *)(&((c)->resbuf[http_client_res(c)->body_off])))
#define	http_client_res_body_l(c)															(http_client_res(c)->body_len)
#define	http_client_res_heads(c, index)														(http_client_res(c)->headers)
#define	http_client_res_head_count(c)														(http_client_res(c)->header_count)
#define	http_client_res_head_i(c, index)													(&(http_client_res(c)->headers[index]))
#define	http_client_res_head(c, name)														http_get_arg((char *)((c)->resbuf), name, http_client_res(c)->headers, http_client_res(c)->header_count)
#define	http_client_res_head_n(c, name, name_l)												http_get_arg_n((char *)((c)->resbuf), name, name_l, http_client_res(c)->headers, http_client_res(c)->header_count)

// Connector Shortcuts
#define	http_client_header(c, name_fmt, val_fmt, ...)										http_connector_header(&((c)->conn), name_fmt, val_fmt, ##__VA_ARGS__)
#define	http_client_header_v(c, name_fmt, val_fmt, args)									http_connector_header_v(&((c)->conn), name_fmt, val_fmt, args)
#define	http_client_header_vp(c, name_fmt, val_fmt, args)									http_connector_header_vp(&((c)->conn), name_fmt, val_fmt, args)
#define	http_client_header_pvp(c, args)														http_connector_header_pvp(&((c)->conn), args)
#define	http_client_printf(c, fmt, ...)														http_connector_printf(&((c)->conn), fmt, ##__VA_ARGS__)
#define	http_client_vprintf(c, fmt, args)													http_connector_vprintf(&((c)->conn), fmt, args)
#define	http_client_vpprintf(c, fmt, args)													http_connector_vpprintf(&((c)->conn), fmt, args)
#define	http_client_printer(c, x)															http_connector_printer(&((c)->conn), x)
#define	http_client_flush(c)																http_connector_flush(&((c)->conn))
#define	http_client_drop(c)																	http_connector_drop(&((c)->conn))

// Partial Declarations
struct http_client;

// Request Complete Callback Type
typedef	void (*http_client_on_complete_t)(void *user, struct http_client *c);

// HTTP Client Structure
struct http_client
{
	// Connector
	struct http_connector conn;

	// State
	uint8_t state;

	// Response
	struct http_res res;

	// Form Buffer
	uint8_t formbuf[HTTP_CLIENT_FORMBUF_SIZE];
	uint16_t formbuf_p;

	// Request Buffer
	uint8_t reqbuf[HTTP_CLIENT_REQBUF_SIZE];

	// Response Buffer
	uint8_t resbuf[HTTP_CLIENT_RESBUF_SIZE];
	uint16_t resbuf_p;

	// Eloop Hook
	struct eloop_hook ehook;

	// On Complete Callback
	http_client_on_complete_t on_complete;
	void *on_complete_user;
};

// Start Generic Request
extern uint8_t http_client_start(struct http_client *c, struct socket_iface *iface, char *method, char *url_fmt, ...);

// Start Generic Request - Variable Argument List
extern uint8_t http_client_start_v(struct http_client *c, struct socket_iface *iface, char *method, char *url_fmt, va_list args);

// Start Generic Request - Variable Argument List Pointer
extern uint8_t http_client_start_vp(struct http_client *c, struct socket_iface *iface, char *method, char *url_fmt, va_list *args);

// End Request
extern void http_client_complete(struct http_client *c);

// Wait until completion in the background (daemonize)
extern void http_client_daemonize(struct http_client *c, http_client_on_complete_t on_complete, void *user);

// Wait until completion in the foreground (wait)
extern void http_client_wait(struct http_client *c);

// Is Request Done
extern uint8_t http_client_finished(struct http_client *c);

// Add Request Body Data
extern void http_client_req_body(struct http_client *c, void *data, uint16_t size);

// Add Form Data
extern void http_client_form_data(struct http_client *c, char *name_fmt, char *val_fmt, ...);

// Add Form Data - Variable Argument List
extern void http_client_form_data_v(struct http_client *c, char *name_fmt, char *val_fmt, va_list args);

// Add Form Data - Variable Argument List Pointer
extern void http_client_form_data_vp(struct http_client *c, char *name_fmt, char *val_fmt, va_list *args);

// Add Form Data - Pure Variable Argument List Pointer
extern void http_client_form_data_pvp(struct http_client *c, va_list *args);

// Perform Generic Data Request
extern uint8_t http_client_req(struct http_client *c, struct socket_iface *iface, char *method, void *data, uint16_t size, char *url_fmt, ...);

// Perform Generic Data Request - Variable Argument List
extern uint8_t http_client_req_v(struct http_client *c, struct socket_iface *iface, char *method, void *data, uint16_t size, char *url_fmt, va_list args);

// Perform Generic Data Request - Variable Argument List Pointer
extern uint8_t http_client_req_vp(struct http_client *c, struct socket_iface *iface, char *method, void *data, uint16_t size, char *url_fmt, va_list *args);

// Process Generic Variable Args Pointer
extern void http_client_process_vpargs(struct http_client *c, uint8_t flags, va_list *args);

// Data Handler (Socket Interface)
extern void http_client_on_recv(struct http_client *c, struct socket *s, uint8_t *data, uint16_t size);

// Disconnect Handler (Socket Interface)
extern void http_client_on_dcnt(struct http_client *c, struct socket *s);

// Eloop Hook Handler
extern void http_client_ehook_handler(struct http_client *c);

#endif
