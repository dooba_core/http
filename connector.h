/* Dooba SDK
 * HTTP Client / Server
 */

#ifndef	__HTTP_CONNECTOR_H
#define	__HTTP_CONNECTOR_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <socket/socket.h>

// HTTP Generic Connector
struct http_connector
{
	// Socket
	struct socket *sck;

	// Data Pointer for Input Buffer
	uint8_t *ibuf;

	// Data Pointer for Output Buffer
	uint8_t *obuf;
	uint16_t obuf_p;
	uint16_t obuf_size;
};

// Print Header
extern void http_connector_header(struct http_connector *c, char *name_fmt, char *val_fmt, ...);

// Print Header - Variable Argument List
extern void http_connector_header_v(struct http_connector *c, char *name_fmt, char *val_fmt, va_list args);

// Print Header - Variable Argument List Pointer
extern void http_connector_header_vp(struct http_connector *c, char *name_fmt, char *val_fmt, va_list *args);

// Print Header - Pure Variable Argument List Pointer
extern void http_connector_header_pvp(struct http_connector *c, va_list *args);

// Print to Connector
extern void http_connector_printf(struct http_connector *c, char *fmt, ...);

// Print to Connector - Variable Argument List
extern void http_connector_vprintf(struct http_connector *c, char *fmt, va_list args);

// Print to Connector - Variable Argument List Pointer
extern void http_connector_vpprintf(struct http_connector *c, char *fmt, va_list *args);

// Connector Printer
extern void http_connector_printer(struct http_connector *c, uint8_t x);

// Flush Connector
extern void http_connector_flush(struct http_connector *c);

// Drop Connector
extern void http_connector_drop(struct http_connector *c);

#endif
