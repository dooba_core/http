/* Dooba SDK
 * HTTP Client / Server
 */

// External Includes
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "http.h"
#include "connector.h"

// Print Header
void http_connector_header(struct http_connector *c, char *name_fmt, char *val_fmt, ...)
{
	va_list ap;

	// Acquire Args & Print
	va_start(ap, val_fmt);
	http_connector_header_vp(c, name_fmt, val_fmt, &ap);
	va_end(ap);
}

// Print Header - Variable Argument List
void http_connector_header_v(struct http_connector *c, char *name_fmt, char *val_fmt, va_list args)
{
	// Print
	http_connector_header_vp(c, name_fmt, val_fmt, &args);
}

// Print Header - Variable Argument List Pointer
void http_connector_header_vp(struct http_connector *c, char *name_fmt, char *val_fmt, va_list *args)
{
	// Add Header
	http_connector_vpprintf(c, name_fmt, args);
	http_connector_printf(c, ": ");
	http_connector_vpprintf(c, val_fmt, args);
	http_connector_printf(c, "\r\n");
}

// Print Header - Pure Variable Argument List Pointer
void http_connector_header_pvp(struct http_connector *c, va_list *args)
{
	char *name_fmt;
	char *val_fmt;

	// Add Header
	name_fmt = (char *)va_arg(*args, char *);
	http_connector_vpprintf(c, name_fmt, args);
	http_connector_printf(c, ": ");
	val_fmt = (char *)va_arg(*args, char *);
	http_connector_vpprintf(c, val_fmt, args);
	http_connector_printf(c, "\r\n");
}

// Print to Connector
void http_connector_printf(struct http_connector *c, char *fmt, ...)
{
	va_list ap;

	// Acquire Args & Print
	va_start(ap, fmt);
	cvprintf((void (*)(void *, uint8_t))http_connector_printer, c, fmt, ap);
	va_end(ap);
}

// Print to Connector - Variable Argument List
void http_connector_vprintf(struct http_connector *c, char *fmt, va_list args)
{
	// Print
	cvprintf((void (*)(void *, uint8_t))http_connector_printer, c, fmt, args);
}

// Print to Connector - Variable Argument List Pointer
void http_connector_vpprintf(struct http_connector *c, char *fmt, va_list *args)
{
	// Print
	cvpprintf((void (*)(void *, uint8_t))http_connector_printer, c, fmt, args);
}

// Connector Printer
void http_connector_printer(struct http_connector *c, uint8_t x)
{
	// Check Socket
	if(!(c->sck))																					{ return; }

	// Free up some space
	if(c->obuf_p >= c->obuf_size)																	{ http_connector_flush(c); }
	if(!(c->sck))																					{ return; }

	// Write
	c->obuf[c->obuf_p] = x;
	c->obuf_p = c->obuf_p + 1;
}

// Flush Connector
void http_connector_flush(struct http_connector *c)
{
	// Check Socket
	if(!(c->sck))																					{ return; }

	// Check Data
	if(c->obuf_p == 0)																				{ return; }

	// Send Data
	if(socket_send(c->sck, c->obuf, c->obuf_p))														{ http_connector_drop(c); }
	c->obuf_p = 0;
}

// Drop Connector
void http_connector_drop(struct http_connector *c)
{
	// Check Socket
	if(!(c->sck))																					{ return; }

	// Drop
	socket_close(c->sck);
	c->sck = 0;
}
