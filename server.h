/* Dooba SDK
 * HTTP Client / Server
 */

#ifndef	__HTTP_SERVER_H
#define	__HTTP_SERVER_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <socket/iface.h>
#include <socket/socket.h>
#include <eloop/eloop.h>

// Internal Includes
#include "http.h"
#include "connector.h"

// Request Buffer Size
#ifndef	HTTP_SERVER_REQBUF_SIZE
#define	HTTP_SERVER_REQBUF_SIZE											512
#endif

// Response Buffer Size
#ifndef	HTTP_SERVER_RESBUF_SIZE
#define	HTTP_SERVER_RESBUF_SIZE											256
#endif

// Max Clients
#ifndef	HTTP_SERVER_MAX_CLIENTS
#define	HTTP_SERVER_MAX_CLIENTS											2
#endif

// Client Timeout
#ifndef	HTTP_SERVER_CLIENT_TIMEOUT
#define	HTTP_SERVER_CLIENT_TIMEOUT										100
#endif

// Update Interval
#ifndef	HTTP_SERVER_UPDATE_INTERVAL
#define	HTTP_SERVER_UPDATE_INTERVAL										100
#endif

// Client Response Status
#define	HTTP_SERVER_RES_STATUS_NONE										0x00
#define	HTTP_SERVER_RES_STATUS_HEAD										0x01
#define	HTTP_SERVER_RES_STATUS_BODY										0x02
#define	HTTP_SERVER_RES_STATUS_DONE										0x03

// HTTP Server Request Shortcuts
#define	http_server_creq(c)												(&((c)->req))
#define	http_server_req_meth_code(c)									(http_server_creq(c)->meth_code)
#define	http_server_req_meth(c)											((char *)((c)->reqbuf))
#define	http_server_req_meth_l(c)										(http_server_creq(c)->meth_len)
#define	http_server_req_ver(c)											((char *)(&((c)->reqbuf[http_server_creq(c)->ver_off])))
#define	http_server_req_ver_l(c)										(http_server_creq(c)->ver_len)
#define	http_server_req_body(c)											((char *)(&((c)->reqbuf[http_server_creq(c)->body_off])))
#define	http_server_req_body_l(c)										(http_server_creq(c)->body_len)

// Server Request URL Shortcuts
#define	http_server_req_url(c)											((char *)(&((c)->reqbuf[http_server_creq(c)->url.url_off])))
#define	http_server_req_url_l(c)										(http_server_creq(c)->url.url_len)
#define	http_server_req_url_obj(c)										(&(http_server_creq(c)->url.obj))
#define	http_server_req_url_port(c)										(http_server_req_url_obj(c)->port)
#define	http_server_req_url_proto(c)									(url_proto(http_server_req_url_obj(c)))
#define	http_server_req_url_proto_l(c)									(http_server_req_url_obj(c)->proto_l)
#define	http_server_req_url_host(c)										(url_host(http_server_req_url_obj(c)))
#define	http_server_req_url_host_l(c)									(http_server_req_url_obj(c)->host_l)
#define	http_server_req_url_path(c)										(url_path(http_server_req_url_obj(c)))
#define	http_server_req_url_path_l(c)									(http_server_req_url_obj(c)->path_l)
#define	http_server_req_url_args(c)										((char *)(&((c)->reqbuf[http_server_creq(c)->url.args_off])))
#define	http_server_req_url_args_l(c)									(http_server_creq(c)->url.args_len)
#define	http_server_req_url_arg_list(c)									(http_server_creq(c)->url.args)
#define	http_server_req_url_arg_count(c)								(http_server_creq(c)->url.arg_count)
#define	http_server_req_url_arg_i(c, index)								(&(http_server_creq(c)->url.args[index]))
#define	http_server_req_url_arg(c, name)								http_get_arg((char *)((c)->reqbuf), name, http_server_creq(c)->url.args, http_server_req_url_arg_count(c))
#define	http_server_req_url_arg_n(c, name, name_l)						http_get_arg_n((char *)((c)->reqbuf), name, name_l, http_server_creq(c)->url.args, http_server_req_url_arg_count(c))

// Server Request Header Shortcuts
#define	http_server_req_heads(c)										(http_server_creq(c)->headers)
#define	http_server_req_head_count(c)									(http_server_creq(c)->header_count)
#define	http_server_req_head_i(c, index)								(&(http_server_creq(c)->headers[index]))
#define	http_server_req_head(c, name)									http_get_arg((char *)((c)->reqbuf), name, http_server_creq(c)->headers, http_server_creq(c)->header_count)
#define	http_server_req_head_n(c, name, name_l)							http_get_arg_n((char *)((c)->reqbuf), name, name_l, http_server_creq(c)->headers, http_server_creq(c)->header_count)

// Server Request Field Shortcuts
#define	http_server_req_fields(c)										(http_server_creq(c)->fields)
#define	http_server_req_field_count(c)									(http_server_creq(c)->field_count)
#define	http_server_req_field_i(c, index)								(&(http_server_creq(c)->fields[index]))
#define	http_server_req_field(c, name)									http_get_arg((char *)((c)->reqbuf), name, http_server_creq(c)->fields, http_server_creq(c)->field_count)
#define	http_server_req_field_n(c, name, name_l)						http_get_arg_n((char *)((c)->reqbuf), name, name_l, http_server_creq(c)->fields, http_server_creq(c)->field_count)

// Connector Shortcuts
#define	http_server_header(c, name_fmt, val_fmt, ...)					http_connector_header(&((c)->conn), name_fmt, val_fmt, ##__VA_ARGS__)
#define	http_server_header_v(c, name_fmt, val_fmt, args)				http_connector_header_v(&((c)->conn), name_fmt, val_fmt, args)
#define	http_server_header_vp(c, name_fmt, val_fmt, args)				http_connector_header_vp(&((c)->conn), name_fmt, val_fmt, args)
#define	http_server_header_pvp(c, args)									http_connector_header_pvp(&((c)->conn), args)
#define	http_server_printf(c, fmt, ...)									http_connector_printf(&((c)->conn), fmt, ##__VA_ARGS__)
#define	http_server_vprintf(c, fmt, args)								http_connector_vprintf(&((c)->conn), fmt, args)
#define	http_server_vpprintf(c, fmt, args)								http_connector_vpprintf(&((c)->conn), fmt, args)
#define	http_server_printer(c, x)										http_connector_printer(&((c)->conn), x)
#define	http_server_flush(c)											http_connector_flush(&((c)->conn))
#define	http_server_drop(c)												http_connector_drop(&((c)->conn))

// HTTP Server Client Structure
struct http_server_client
{
	// Connector
	struct http_connector conn;

	// Timer
	uint16_t timer;

	// Request Complete
	uint8_t req_complete;

	// Continue Sent
	uint8_t continue_sent;

	// Response Status
	uint8_t res_status;

	// Request
	struct http_req req;

	// Request Buffer
	uint8_t reqbuf[HTTP_SERVER_REQBUF_SIZE];
	uint16_t reqbuf_p;

	// Response Buffer
	uint8_t resbuf[HTTP_SERVER_RESBUF_SIZE];
};

// Request Handler Type
typedef	void (*http_server_req_handler_t)(void *user, struct http_server_client *c);

// HTTP Server Structure
struct http_server
{
	// Socket
	struct socket *sck;

	// Data Received
	uint8_t data_rxd;

	// Update Timer
	uint16_t update_timer;

	// Clients
	struct http_server_client clients[HTTP_SERVER_MAX_CLIENTS];

	// Request Handler
	http_server_req_handler_t req_handler;

	// User Data
	void *user;

	// Eloop Hook
	struct eloop_hook ehook;
	uint8_t ehook_on;
};

// Create HTTP Server
extern uint8_t http_server_start(struct http_server *srv, uint16_t port, struct socket_iface *iface, http_server_req_handler_t req_handler, void *user);

// Drop HTTP Server
extern void http_server_stop(struct http_server *srv);

// Daemonize (Run server in background)
extern void http_server_daemonize(struct http_server *srv);

// Update HTTP Server
extern void http_server_update(struct http_server *srv);

// Start Response
extern void http_server_respond(struct http_server_client *c, uint16_t code);

// Complete Response
extern void http_server_resp_end(struct http_server_client *c);

// Add Response Body
extern void http_server_resp_body(struct http_server_client *c, void *body);

// Add Response Body - Fixed Size
extern void http_server_resp_body_n(struct http_server_client *c, void *body, uint16_t body_len);

// Redirect
extern void http_server_redirect(struct http_server_client *c, char *fmt, ...);

// Connect Handler (Socket Interface)
extern void http_server_on_cnct(struct http_server *srv, struct socket *s);

// Data Handler (Socket Interface)
extern void http_server_on_recv(struct http_server *srv, struct socket *s, uint8_t *data, uint16_t size);

// Disconnect Handler (Socket Interface)
extern void http_server_on_dcnt(struct http_server *srv, struct socket *s);

#endif
