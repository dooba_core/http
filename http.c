/* Dooba SDK
 * HTTP Client / Server
 */

// External Includes
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "http.h"

// Split HTTP Request
uint8_t http_split_req(char *s, uint16_t l, struct http_req *r)
{
	int16_t rest_l = 0;
	int16_t sl = 0;
	uint16_t t_end = 0;
	uint16_t p = 0;
	int16_t es1;
	int16_t es2;
	uint16_t t_end1 = 0;
	uint16_t t_end2 = 0;
	char *d;

	// Clear Request
	r->meth_len = 0;
	r->meth_code = HTTP_METH_CODE_NONE;
	r->ver_off = 0;
	r->ver_len = 0;
	r->body_off = 0;
	r->body_len = 0;
	r->header_count = 0;
	r->field_count = 0;

	// Clear Request URL
	r->url.url_off = 0;
	r->url.url_len = 0;
	r->url.obj_len = 0;
	r->url.args_off = 0;
	r->url.args_len = 0;
	r->url.arg_count = 0;

	// Start
	p = 0;
	rest_l = l;

	// Extract Command
	sl = str_find(&(s[p]), rest_l, HTTP_DELIM, &t_end);
	if(sl < 0)																							{ return 1; }

	// Set Command
	r->meth_len = sl;
	r->meth_code = http_meth2code(s, sl);

	// Check Command Code
	if(r->meth_code == HTTP_METH_CODE_NONE)																{ r->meth_len = 0; return 1; }

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;
	if(rest_l <= 0)																						{ return 1; }

	// Acquire URL
	sl = str_find(&(s[p]), rest_l, HTTP_DELIM, &t_end);
	if(sl < 0)																							{ return 1; }

	// Split URL
	if(http_split_url(s, p, sl, &(r->url)))																{ return 1; }

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;
	if(rest_l <= 0)																						{ return 1; }

	// Acquire Version
	sl = str_find(&(s[p]), rest_l, "\n", &t_end);
	if(sl < 0)																							{ return 1; }

	// Set Version
	r->ver_off = p;
	r->ver_len = sl;
	if(r->ver_len)																						{ if(s[r->ver_off + (r->ver_len - 1)] == '\r') { r->ver_len = r->ver_len - 1; } }

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;
	if(rest_l <= 0)																						{ return 1; }

	// Detect Line Ending Style
	es1 = str_find(&(s[p]), rest_l, "\r\n\r\n", &t_end1);
	es2 = str_find(&(s[p]), rest_l, "\n\n", &t_end2);
	if(es1 == es2)																						{ return 1; }
	if(((es1 < es2) && (es1 > 0)) || ((es1 > es2) && (es2 < 0)))										{ sl = es1; d = "\r\n"; t_end = t_end1; }
	if(((es2 < es1) && (es2 > 0)) || ((es2 > es1) && (es1 < 0)))										{ sl = es2; d = "\n"; t_end = t_end2; }

	// Split Headers
	http_split_head(s, p, sl, d, r->headers, HTTP_HEADERS_MAX, &(r->header_count));

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;

	// Set Body
	r->body_off = p;
	r->body_len = rest_l;

	// Attempt to Parse Body as Form
	if(str_find(&(s[r->body_off]), r->body_len, "\n", 0) < 0)											{ http_split_form(s, r->body_off, r->body_len, r->fields, HTTP_FORM_FIELDS_MAX, &(r->field_count)); }

	return 0;
}

// Split HTTP Response
uint8_t http_split_res(char *s, uint16_t l, struct http_res *r)
{
	int16_t rest_l = 0;
	int16_t sl = 0;
	uint16_t t_end = 0;
	uint16_t p = 0;
	int16_t es1;
	int16_t es2;
	uint16_t t_end1 = 0;
	uint16_t t_end2 = 0;
	char *d;

	// Clear Response
	r->ver_len = 0;
	r->status_off = 0;
	r->status_len = 0;
	r->status_txt_off = 0;
	r->status_txt_len = 0;
	r->status = 0;
	r->body_off = 0;
	r->body_len = 0;
	r->header_count = 0;

	// Start
	p = 0;
	rest_l = l;

	// Extract Version
	sl = str_find(&(s[p]), rest_l, HTTP_DELIM, &t_end);
	if(sl < 0)																							{ return 1; }
	r->ver_len = sl;

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;
	if(rest_l <= 0)																						{ return 1; }

	// Acquire Status
	sl = str_find(&(s[p]), rest_l, "\n", &t_end);
	if(sl < 0)																							{ return 1; }
	r->status_off = p;
	r->status_len = sl;
	if(r->status_len)																					{ if(s[r->status_off + (r->status_len - 1)] == '\r') { r->status_len = r->status_len - 1; } }
	r->status = atoi(&(s[r->status_off]));

	// Acquire Status Text
	if(str_find(&(s[p]), r->status_len, HTTP_DELIM, &(r->status_txt_off)) >= 0)							{ r->status_txt_len = r->status_len - r->status_txt_off; r->status_txt_off = p + r->status_txt_off; }

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;
	if(rest_l <= 0)																						{ return 1; }

	// Detect Line Ending Style
	es1 = str_find(&(s[p]), rest_l, "\r\n\r\n", &t_end1);
	es2 = str_find(&(s[p]), rest_l, "\n\n", &t_end2);
	if(es1 == es2)																						{ return 1; }
	if(((es1 < es2) && (es1 > 0)) || ((es1 > es2) && (es2 < 0)))										{ sl = es1; d = "\r\n"; t_end = t_end1; }
	if(((es2 < es1) && (es2 > 0)) || ((es2 > es1) && (es1 < 0)))										{ sl = es2; d = "\n"; t_end = t_end2; }

	// Split Headers
	http_split_head(s, p, sl, d, r->headers, HTTP_HEADERS_MAX, &(r->header_count));

	// NEXT
	p = p + t_end;
	rest_l = rest_l - t_end;

	// Set Body
	r->body_off = p;
	r->body_len = rest_l;

	return 0;
}

// Split HTTP URL
uint8_t http_split_url(char *s, uint8_t off, uint16_t l, struct http_url *u)
{
	int16_t rest_l = 0;
	int16_t sl = 0;
	uint16_t t_end = 0;
	uint16_t p = 0;

	// Init URL
	u->url_off = off;
	u->url_len = l;
	u->obj_len = l;
	u->args_off = 0;
	u->args_len = 0;
	u->arg_count = 0;

	// Start
	p = off;
	rest_l = l;

	// Extract URL Object
	sl = str_find(&(s[p]), rest_l, HTTP_URL_ARGS_DELIM, &t_end);
	if(sl > 0)
	{
		// Set URL Object
		u->obj_len = sl;
		u->args_off = p + t_end;
		u->args_len = rest_l - t_end;

		// Split URL Args
		if(u->args_len > 0)																				{ http_split_args(s, u->args_off, u->args_len, 0, u->args, HTTP_URL_MAX_ARGS, &(u->arg_count)); }
	}

	// Split URL Object
	url_split_n(&(u->obj), &(s[u->url_off]), u->obj_len);

	return 0;
}

// Split HTTP Headers
void http_split_head(char *s, uint16_t off, uint16_t l, char *line_delim, struct http_arg *h, uint8_t size, uint8_t *header_count)
{
	struct http_arg_splitter splitter;

	// Prep Splitter
	splitter.args = h;
	splitter.size = size;
	splitter.count = 0;
	*header_count = 0;

	// Split
	str_multi_split(s, off, l, 0, line_delim, HTTP_HEADER_VAL_DELIM, (void (*)(void *, uint16_t, uint16_t, uint16_t, uint16_t))http_arg_splitter, &splitter);

	// Set Count
	*header_count = splitter.count;
}

// Split HTTP Form Content
void http_split_form(char *s, uint16_t off, uint16_t l, struct http_arg *args, uint8_t size, uint8_t *field_count)
{
	// Split Args
	http_split_args(s, off, l, 0, args, size, field_count);
}

// Split Generic Arguments
void http_split_args(char *s, uint16_t off, uint16_t l, uint16_t *p, struct http_arg *args, uint8_t size, uint8_t *count)
{
	struct http_arg_splitter splitter;

	// Prep Splitter
	splitter.args = args;
	splitter.size = size;
	splitter.count = 0;

	// Split
	str_multi_split(s, off, l, p, HTTP_ARG_DELIM, HTTP_ARGVAL_DELIM, (void (*)(void *, uint16_t, uint16_t, uint16_t, uint16_t))http_arg_splitter, &splitter);

	// Set Count
	*count = splitter.count;
}

// Generic Argument Splitter
void http_arg_splitter(struct http_arg_splitter *s, uint16_t name, uint16_t name_l, uint16_t value, uint16_t value_l)
{
	// Set Argument
	http_set_arg(name, name_l, value, value_l, s->args, s->size, &(s->count));
}

// Get Argument from List
struct http_arg *http_get_arg(char *s, char *name, struct http_arg *args, uint8_t count)
{
	uint8_t i;

	// Run through Arguments
	for(i = 0; i < count; i = i + 1)																	{ if(strncasecmp(&(s[args[i].name_off]), name, args[i].name_len) == 0) { return &(args[i]); } }

	return 0;
}

// Set Argument
void http_set_arg(uint16_t name, uint16_t name_l, uint16_t val, uint16_t val_l, struct http_arg *args, uint8_t size, uint8_t *arg_count)
{
	// Check Count
	if(*arg_count >= size)																				{ return; }

	// Set Arg
	args[*arg_count].name_off = name;
	args[*arg_count].name_len = name_l;
	args[*arg_count].val_off = val;
	args[*arg_count].val_len = val_l;

	// Inc Count
	*arg_count = *arg_count + 1;
}

// Code to Command
char *http_code2meth(uint8_t code)
{
	// Check Code
	if(code == HTTP_METH_CODE_HEAD)																		{ return HTTP_METH_HEAD; }
	if(code == HTTP_METH_CODE_GET)																		{ return HTTP_METH_GET; }
	if(code == HTTP_METH_CODE_POST)																		{ return HTTP_METH_POST; }
	if(code == HTTP_METH_CODE_PUT)																		{ return HTTP_METH_PUT; }
	if(code == HTTP_METH_CODE_PATCH)																	{ return HTTP_METH_PATCH; }
	if(code == HTTP_METH_CODE_DELETE)																	{ return HTTP_METH_DELETE; }
	if(code == HTTP_METH_CODE_OPTIONS)																	{ return HTTP_METH_OPTIONS; }

	return HTTP_METH_UNK;
}

// Command to Code
uint8_t http_meth2code(char *meth, uint8_t len)
{
	// Check Command
	if(strncasecmp(meth, HTTP_METH_HEAD, len) == 0)														{ return HTTP_METH_CODE_HEAD; }
	if(strncasecmp(meth, HTTP_METH_GET, len) == 0)														{ return HTTP_METH_CODE_GET; }
	if(strncasecmp(meth, HTTP_METH_POST, len) == 0)														{ return HTTP_METH_CODE_POST; }
	if(strncasecmp(meth, HTTP_METH_PUT, len) == 0)														{ return HTTP_METH_CODE_PUT; }
	if(strncasecmp(meth, HTTP_METH_PATCH, len) == 0)													{ return HTTP_METH_CODE_PATCH; }
	if(strncasecmp(meth, HTTP_METH_DELETE, len) == 0)													{ return HTTP_METH_CODE_DELETE; }
	if(strncasecmp(meth, HTTP_METH_OPTIONS, len) == 0)													{ return HTTP_METH_CODE_OPTIONS; }

	return HTTP_METH_CODE_NONE;
}

// Code to Response
char *http_code2resp(uint16_t code)
{
	// Check Code
	if(code == HTTP_RES_OK)																				{ return HTTP_RESPONSE_OK; }
	if(code == HTTP_RES_FOUND)																			{ return HTTP_RESPONSE_FOUND; }
	if(code == HTTP_RES_UNAUTHORIZED)																	{ return HTTP_RESPONSE_UNAUTHORIZED; }
	if(code == HTTP_RES_NOTFOUND)																		{ return HTTP_RESPONSE_NOTFOUND; }
	if(code == HTTP_RES_UNPROCESSABLE)																	{ return HTTP_RESPONSE_UNPROCESSABLE; }
	if(code == HTTP_RES_SRVERROR)																		{ return HTTP_RESPONSE_SRVERROR; }

	return "";
}

// URL Encode
int16_t http_urlenc(char *buf, uint16_t buf_l, char *fmt, ...)
{
	va_list ap;
	int16_t r;

	// Acquire Args
	va_start(ap, fmt);

	// URL Encode
	r = http_urlenc_v(buf, buf_l, fmt, &ap);

	// Release Args
	va_end(ap);

	return r;
}

// URL Encode - Variable Argument List
int16_t http_urlenc_v(char *buf, uint16_t buf_l, char *fmt, va_list args)
{
	// Encode
	return http_urlenc_vp(buf, buf_l, fmt, &args);
}

// URL Encode - Variable Argument List Pointer
int16_t http_urlenc_vp(char *buf, uint16_t buf_l, char *fmt, va_list *args)
{
	struct http_urlenc_printer_info i;

	// Setup Printer
	i.buf = buf;
	i.buf_l = buf_l;
	i.buf_p = 0;
	i.overflow = 0;

	// Print
	cvpprintf((void (*)(void *, uint8_t))http_urlenc_printer, &i, fmt, args);
	if(i.overflow)																						{ return -1; }

	return i.buf_p;
}

// URL Encode Printer
void http_urlenc_printer(struct http_urlenc_printer_info *i, uint8_t x)
{
	uint8_t p;

	// Check Size
	if(i->buf_p >= i->buf_l)																			{ i->overflow = 1; return; }

	// Write
	if(((x < '0') || (x > '9')) && ((x < 'a') || (x > 'z')) && ((x < 'A') || (x > 'Z')) && (x != '-') && (x != '_') && (x != '[') && (x != ']'))
	{
		if((i->buf_p + 3) > i->buf_l)																	{ i->overflow = 1; return; }
		i->buf[i->buf_p] = '%';
		p = (x >> 4) & 0x0f;
		i->buf[i->buf_p + 1] = (p > 9) ? ('a' + (p - 10)) : ('0' + p);
		p = x & 0x0f;
		i->buf[i->buf_p + 2] = (p > 9) ? ('a' + (p - 10)) : ('0' + p);
		i->buf_p = i->buf_p + 3;
	}
	else																								{ i->buf[i->buf_p] = x; i->buf_p = i->buf_p + 1; }
}

// URL Decode
int16_t http_urldec(char *buf, uint16_t buf_l, char *s, uint16_t l)
{
	uint16_t i;
	uint16_t j;
	uint8_t x;
	char sb[3];

	i = 0;
	j = 0;
	while((i < l) && (j < buf_l))
	{
		// Read Next Element
		x = s[i];
		if(x == '%')																					{ if((i + 3) <= l) { memcpy(sb, &(s[i + 1]), 2); sb[2] = 0; x = strtoul(sb, 0, 16); i = i + 3; } else { return -1; } }
		else																							{ i = i + 1; }

		// Write
		buf[j] = x;
		j = j + 1;
	}
	if(i < l)																							{ return -1; }

	return j;
}
