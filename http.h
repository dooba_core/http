/* Dooba SDK
 * HTTP Client / Server
 */

#ifndef	__HTTP_H
#define	__HTTP_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <url/url.h>

// Port
#define	HTTP_PORT										80

// Protocol Name
#define	HTTP_PROTO										"http"

// Methods
#define	HTTP_METH_UNK									"???"
#define	HTTP_METH_GET									"GET"
#define	HTTP_METH_HEAD									"HEAD"
#define	HTTP_METH_POST									"POST"
#define	HTTP_METH_PUT									"PUT"
#define	HTTP_METH_DELETE								"DELETE"
#define	HTTP_METH_OPTIONS								"OPTIONS"
#define	HTTP_METH_PATCH									"PATCH"

// Method Codes
#define	HTTP_METH_CODE_NONE								0x00
#define	HTTP_METH_CODE_GET								0x01
#define	HTTP_METH_CODE_HEAD								0x02
#define	HTTP_METH_CODE_POST								0x03
#define	HTTP_METH_CODE_PUT								0x04
#define	HTTP_METH_CODE_DELETE							0x05
#define	HTTP_METH_CODE_OPTIONS							0x06
#define	HTTP_METH_CODE_PATCH							0x07

// Responses
#define	HTTP_RESPONSE_CONTINUE							"Continue"
#define	HTTP_RESPONSE_OK								"OK"
#define	HTTP_RESPONSE_FOUND								"Found"
#define	HTTP_RESPONSE_UNAUTHORIZED						"Unauthorized"
#define	HTTP_RESPONSE_NOTFOUND							"Not Found"
#define	HTTP_RESPONSE_UNPROCESSABLE						"Unprocessable Entity"
#define	HTTP_RESPONSE_SRVERROR							"Server Error"

// Response Codes
#define	HTTP_RES_CONTINUE								100
#define	HTTP_RES_OK										200
#define	HTTP_RES_FOUND									302
#define	HTTP_RES_UNAUTHORIZED							401
#define	HTTP_RES_NOTFOUND								404
#define	HTTP_RES_UNPROCESSABLE							422
#define	HTTP_RES_SRVERROR								500

// Basic Delimiter
#define	HTTP_DELIM										" "

// URL Arguments Delimiter
#define	HTTP_URL_ARGS_DELIM								"?"

// Argument Delimiter
#define	HTTP_ARG_DELIM									"&"

// Argument Value Delimiter
#define	HTTP_ARGVAL_DELIM								"="

// Header Value Delimiter
#define	HTTP_HEADER_VAL_DELIM							": "

// Common Headers
#define	HTTP_HEADER_HOST								"Host"
#define	HTTP_HEADER_SERVER								"Server"
#define	HTTP_HEADER_CONTENTLENGTH						"Content-Length"
#define	HTTP_HEADER_CONTENTTYPE							"Content-Type"
#define	HTTP_HEADER_CONNECTION							"Connection"
#define	HTTP_HEADER_COOKIE								"Cookie"
#define	HTTP_HEADER_ACCEPT								"Accept"
#define	HTTP_HEADER_LOCATION							"Location"
#define	HTTP_HEADER_AUTHORIZATION						"Authorization"
#define	HTTP_HEADER_USERAGENT							"User-Agent"
#define	HTTP_HEADER_EXPECT								"Expect"
#define	HTTP_HEADER_HSTS								"Strict-Transport-Security"

// Header - Content Types
#define	HTTP_HEADER_CONTENTTYPE_TEXTPLAIN				"text/plain"
#define	HTTP_HEADER_CONTENTTYPE_APPJSON					"application/json"
#define	HTTP_HEADER_CONTENTTYPE_APPFORMURL				"application/x-www-form-urlencoded"

// Header - Connection Types
#define	HTTP_HEADER_CONNECTIONTYPE_CLOSE				"close"

// Max URL Arguments
#ifndef	HTTP_URL_MAX_ARGS
#define	HTTP_URL_MAX_ARGS								10
#endif

// Max Headers
#ifndef	HTTP_HEADERS_MAX
#define	HTTP_HEADERS_MAX								10
#endif

// Max Form Fields
#ifndef	HTTP_FORM_FIELDS_MAX
#define	HTTP_FORM_FIELDS_MAX							10
#endif

// HTTP Generic Argument Shortcuts
#define	http_arg_name(r, a)								((char *)(&((r)->conn.ibuf[a->name_off])))
#define	http_arg_name_l(r, a)							(a->name_len)
#define	http_arg_val(r, a)								((char *)(&((r)->conn.ibuf[a->val_off])))
#define	http_arg_val_l(r, a)							(a->val_len)

// HTTP Argument Structure
struct http_arg
{
	// Name
	uint16_t name_off;
	uint16_t name_len;

	// Value
	uint16_t val_off;
	uint16_t val_len;
};

// HTTP URL Structure
struct http_url
{
	// URL
	uint8_t url_off;
	uint16_t url_len;

	// Object
	uint16_t obj_len;
	struct url obj;

	// Arguments
	uint16_t args_off;
	uint16_t args_len;
	struct http_arg args[HTTP_URL_MAX_ARGS];
	uint8_t arg_count;
};

// HTTP Request Structure
struct http_req
{
	// Command
	uint8_t meth_len;
	uint8_t meth_code;

	// Version
	uint16_t ver_off;
	uint8_t ver_len;

	// Body
	uint16_t body_off;
	uint16_t body_len;

	// URL
	struct http_url url;

	// Headers
	struct http_arg headers[HTTP_HEADERS_MAX];
	uint8_t header_count;

	// Form Fields
	struct http_arg fields[HTTP_FORM_FIELDS_MAX];
	uint8_t field_count;
};

// HTTP Response
struct http_res
{
	// Version
	uint8_t ver_len;

	// Status
	uint8_t status_off;
	uint16_t status_len;
	uint16_t status_txt_off;
	uint16_t status_txt_len;
	uint16_t status;

	// Body
	uint16_t body_off;
	uint16_t body_len;

	// Headers
	struct http_arg headers[HTTP_HEADERS_MAX];
	uint8_t header_count;
};

// HTTP Argument Splitter Structure
struct http_arg_splitter
{
	// Args
	struct http_arg *args;

	// Size
	uint8_t size;

	// Count
	uint8_t count;
};

// URL Encode Printer Info Structure
struct http_urlenc_printer_info
{
	// Buffer
	char *buf;

	// Buffer Size
	uint16_t buf_l;

	// Position
	uint16_t buf_p;

	// Overflow
	uint8_t overflow;
};

// Split HTTP Request
extern uint8_t http_split_req(char *s, uint16_t l, struct http_req *r);

// Split HTTP Response
extern uint8_t http_split_res(char *s, uint16_t l, struct http_res *r);

// Split HTTP URL
extern uint8_t http_split_url(char *s, uint8_t off, uint16_t l, struct http_url *u);

// Split HTTP Headers
extern void http_split_head(char *s, uint16_t off, uint16_t l, char *line_delim, struct http_arg *h, uint8_t size, uint8_t *header_count);

// Split HTTP Form Content
extern void http_split_form(char *s, uint16_t off, uint16_t l, struct http_arg *args, uint8_t size, uint8_t *field_count);

// Split Generic Arguments
extern void http_split_args(char *s, uint16_t off, uint16_t l, uint16_t *p, struct http_arg *args, uint8_t size, uint8_t *count);

// Generic Argument Splitter
extern void http_arg_splitter(struct http_arg_splitter *s, uint16_t name, uint16_t name_l, uint16_t value, uint16_t value_l);

// Get Argument from List
extern struct http_arg *http_get_arg(char *s, char *name, struct http_arg *args, uint8_t count);

// Get Argument from List - Fixed Length
extern struct http_arg *http_get_arg_n(char *s, char *name, uint16_t name_l, struct http_arg *args, uint8_t count);

// Set Argument
extern void http_set_arg(uint16_t name, uint16_t name_l, uint16_t val, uint16_t val_l, struct http_arg *args, uint8_t size, uint8_t *arg_count);

// Code to Method
extern char *http_code2meth(uint8_t code);

// Method to Code
extern uint8_t http_meth2code(char *meth, uint8_t len);

// Code to Response
extern char *http_code2resp(uint16_t code);

// URL Encode
extern int16_t http_urlenc(char *buf, uint16_t buf_l, char *fmt, ...);

// URL Encode - Variable Argument List
extern int16_t http_urlenc_v(char *buf, uint16_t buf_l, char *fmt, va_list args);

// URL Encode - Variable Argument List Pointer
extern int16_t http_urlenc_vp(char *buf, uint16_t buf_l, char *fmt, va_list *args);

// URL Encode Printer
extern void http_urlenc_printer(struct http_urlenc_printer_info *i, uint8_t x);

// URL Decode
extern int16_t http_urldec(char *buf, uint16_t buf_l, char *s, uint16_t l);

#endif
