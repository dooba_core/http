/* Dooba SDK
 * HTTP Client / Server
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include <util/cprintf.h>
#include <util/str.h>
#include <url/url.h>

// Internal Includes
#include "client.h"

// Start Generic Request
uint8_t http_client_start(struct http_client *c, struct socket_iface *iface, char *method, char *url_fmt, ...)
{
	va_list ap;
	uint8_t res;

	// Acquire Args & Start Req
	va_start(ap, url_fmt);
	res = http_client_start_vp(c, iface, method, url_fmt, &ap);
	va_end(ap);

	return res;
}

// Start Generic Request - Variable Argument List
uint8_t http_client_start_v(struct http_client *c, struct socket_iface *iface, char *method, char *url_fmt, va_list args)
{
	// Start Req
	return http_client_start_vp(c, iface, method, url_fmt, &args);
}

// Start Generic Request - Variable Argument List Pointer
uint8_t http_client_start_vp(struct http_client *c, struct socket_iface *iface, char *method, char *url_fmt, va_list *args)
{
	struct url u;
	struct socket **s;

	// Render URL
	c->formbuf_p = cvpsnprintf(c->formbuf, HTTP_CLIENT_FORMBUF_SIZE, url_fmt, args);
	if(c->formbuf_p == 0)																														{ return 1; }

	// Parse URL
	url_split_n(&u, (char *)(c->formbuf), c->formbuf_p);

	// Check URL
	if(u.host_l == 0)																															{ return 1; }
	if(u.path_l == 0)																															{ return 1; }
	if(str_cmp(url_proto(&u), u.proto_l, HTTP_PROTO, strlen(HTTP_PROTO)))																		{ return 1; }
	if(u.port == 0)																																{ u.port = HTTP_PORT; }

	// Prepare Request Structure
	c->conn.sck = 0;
	c->conn.ibuf = c->resbuf;
	c->conn.obuf = c->reqbuf;
	c->conn.obuf_p = 0;
	c->conn.obuf_size = HTTP_CLIENT_REQBUF_SIZE;
	c->state = HTTP_CLIENT_REQ_STATE_NONE;
	c->formbuf_p = 0;
	c->resbuf_p = 0;
	c->on_complete = 0;
	c->on_complete_user = 0;

	// Attempt to Open Socket
	s = &(c->conn.sck);
	if(socket_cnct_u(iface, s, &u, c, (socket_recv_handler_t)http_client_on_recv, (socket_dcnt_handler_t)http_client_on_dcnt))					{ return 1; }

	// Issue Command
	http_client_printf(c, "%s ", method);
	http_client_printf(c, "%t ", url_path(&u), u.path_l);
	http_client_printf(c, "HTTP/1.1\r\n");

	// Issue Basic Headers
	c->state = HTTP_CLIENT_REQ_STATE_HEAD;
	http_client_header(c, HTTP_HEADER_HOST, "%t", url_host(&u), u.host_l);
	http_client_header(c, HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTIONTYPE_CLOSE);

	return 0;
}

// End Request
void http_client_complete(struct http_client *c)
{
	// Check Request State
	if((c->state != HTTP_CLIENT_REQ_STATE_HEAD) && (c->state != HTTP_CLIENT_REQ_STATE_FORM) && (c->state != HTTP_CLIENT_REQ_STATE_BODY))		{ return; }

	// Handle Head State
	if(c->state == HTTP_CLIENT_REQ_STATE_HEAD)																									{ http_client_printf(c, "\r\n"); }

	// Handle Form Data
	else if(c->state == HTTP_CLIENT_REQ_STATE_FORM)
	{
		// Complete Headers
		c->state = HTTP_CLIENT_REQ_STATE_HEAD;
		http_client_header(c, HTTP_HEADER_CONTENTTYPE, HTTP_HEADER_CONTENTTYPE_APPFORMURL);
		http_client_header(c, HTTP_HEADER_CONTENTLENGTH, "%i", c->formbuf_p);
		http_client_printf(c, "\r\n");
		http_client_printf(c, "%t", c->formbuf, c->formbuf_p);
	}

	// Be nice...
	else																																		{ /* NoOp */ }

	// Flush
	c->state = HTTP_CLIENT_REQ_STATE_RESP;
	http_client_flush(c);
}

// Wait until completion in the background (daemonize)
void http_client_daemonize(struct http_client *c, http_client_on_complete_t on_complete, void *user)
{
	// Set Completion Callback
	c->on_complete = on_complete;
	c->on_complete_user = user;

	// Hook Eloop
	eloop_hook(&(c->ehook), (eloop_hook_t)http_client_ehook_handler, c);
}

// Wait until completion in the foreground (wait)
void http_client_wait(struct http_client *c)
{
	// Active wait
	while(http_client_finished(c) == 0)																											{ eloop_update(); }

	// Notify
	if(c->on_complete)																															{ c->on_complete(c->on_complete_user, c); }
}

// Is Request Done
uint8_t http_client_finished(struct http_client *c)
{
	// Check Done & Drop Socket
	if((c->state == HTTP_CLIENT_REQ_STATE_FAIL) || (c->state == HTTP_CLIENT_REQ_STATE_DONE))													{ http_client_drop(c); return 1; }

	return 0;
}

// Add Request Body Data
void http_client_req_body(struct http_client *c, void *data, uint16_t size)
{
	// Check State
	if(c->state != HTTP_CLIENT_REQ_STATE_HEAD)																									{ return; }

	// Complete Headers
	http_client_header(c, HTTP_HEADER_CONTENTLENGTH, "%i", size);

	// Add Body
	c->state = HTTP_CLIENT_REQ_STATE_BODY;
	http_client_printf(c, "\r\n");
	http_client_printf(c, "%t", data, size);
}

// Add Form Data
void http_client_form_data(struct http_client *c, char *name_fmt, char *val_fmt, ...)
{
	va_list ap;

	// Acquire Args & Print
	va_start(ap, val_fmt);
	http_client_form_data_vp(c, name_fmt, val_fmt, &ap);
	va_end(ap);
}

// Add Form Data - Variable Argument List
void http_client_form_data_v(struct http_client *c, char *name_fmt, char *val_fmt, va_list args)
{
	// Print
	http_client_form_data_vp(c, name_fmt, val_fmt, &args);
}

// Add Form Data - Variable Argument List Pointer
void http_client_form_data_vp(struct http_client *c, char *name_fmt, char *val_fmt, va_list *args)
{
	int16_t l;

	// Check Request State & Buffer
	if(c->state == HTTP_CLIENT_REQ_STATE_HEAD)																									{ c->state = HTTP_CLIENT_REQ_STATE_FORM; }
	if(c->state != HTTP_CLIENT_REQ_STATE_FORM)																									{ return; }
	if(c->formbuf_p >= HTTP_CLIENT_FORMBUF_SIZE)																								{ return; }

	// Add Delimiter
	if(c->formbuf_p)																															{ c->formbuf[c->formbuf_p] = '&'; c->formbuf_p = c->formbuf_p + 1; }

	// Write Name
	c->formbuf_p = c->formbuf_p + cvpsnprintf(&(c->formbuf[c->formbuf_p]), HTTP_CLIENT_FORMBUF_SIZE - c->formbuf_p, name_fmt, args);
	c->formbuf_p = c->formbuf_p + csnprintf(&(c->formbuf[c->formbuf_p]), HTTP_CLIENT_FORMBUF_SIZE - c->formbuf_p, "=");

	// URL Encode Value
	l = http_urlenc_vp((char *)&(c->formbuf[c->formbuf_p]), HTTP_CLIENT_FORMBUF_SIZE - c->formbuf_p, val_fmt, args);
	if(l < 0)																																	{ return; }
	c->formbuf_p = c->formbuf_p + l;
}

// Add Form Data - Pure Variable Argument List Pointer
void http_client_form_data_pvp(struct http_client *c, va_list *args)
{
	char *name_fmt;
	char *val_fmt;
	int16_t l;

	// Check Request State & Buffer
	if(c->state == HTTP_CLIENT_REQ_STATE_HEAD)																									{ c->state = HTTP_CLIENT_REQ_STATE_FORM; }
	if(c->state != HTTP_CLIENT_REQ_STATE_FORM)																									{ return; }
	if(c->formbuf_p >= HTTP_CLIENT_FORMBUF_SIZE)																								{ return; }

	// Add Delimiter
	if(c->formbuf_p)																															{ c->formbuf[c->formbuf_p] = '&'; c->formbuf_p = c->formbuf_p + 1; }

	// Write Name
	name_fmt = (char *)va_arg(*args, char *);
	c->formbuf_p = c->formbuf_p + cvpsnprintf(&(c->formbuf[c->formbuf_p]), HTTP_CLIENT_FORMBUF_SIZE - c->formbuf_p, name_fmt, args);
	c->formbuf_p = c->formbuf_p + csnprintf(&(c->formbuf[c->formbuf_p]), HTTP_CLIENT_FORMBUF_SIZE - c->formbuf_p, "=");

	// URL Encode Value
	val_fmt = (char *)va_arg(*args, char *);
	l = http_urlenc_vp((char *)&(c->formbuf[c->formbuf_p]), HTTP_CLIENT_FORMBUF_SIZE - c->formbuf_p, val_fmt, args);
	if(l < 0)																																	{ return; }
	c->formbuf_p = c->formbuf_p + l;
}

// Perform Generic Data Request
uint8_t http_client_req(struct http_client *c, struct socket_iface *iface, char *method, void *data, uint16_t size, char *url_fmt, ...)
{
	va_list ap;
	uint8_t res;

	// Acquire Args & Start Req
	va_start(ap, url_fmt);
	res = http_client_req_vp(c, iface, method, data, size, url_fmt, &ap);
	va_end(ap);

	return res;
}

// Perform Generic Data Request - Variable Argument List
uint8_t http_client_req_v(struct http_client *c, struct socket_iface *iface, char *method, void *data, uint16_t size, char *url_fmt, va_list args)
{
	// Do Req
	return http_client_req_vp(c, iface, method, data, size, url_fmt, &args);
}

// Perform Generic Data Request - Variable Argument List Pointer
uint8_t http_client_req_vp(struct http_client *c, struct socket_iface *iface, char *method, void *data, uint16_t size, char *url_fmt, va_list *args)
{
	// Start Request
	if(http_client_start_vp(c, iface, method, url_fmt, args))																					{ return 1; }
	else
	{
		// Process Generic Arguments (Only allow Form Data if No Body)
		http_client_process_vpargs(c, HTTP_CLIENT_VARGS_HEADERS | (size ? 0 : HTTP_CLIENT_VARGS_FORMDATA), args);

		// Set Body
		if(size)																																{ http_client_req_body(c, data, size); }

		// Complete Request
		http_client_complete(c);
	}

	return 0;
}

// Process Generic Variable Args Pointer
void http_client_process_vpargs(struct http_client *c, uint8_t flags, va_list *args)
{
	int v;

	// Run through Arguments
	v = 1;
	while(v)
	{
		// Parse Arguments
		v = va_arg(*args, int);
		if((v == HTTP_CLIENT_HEADER) && (flags & HTTP_CLIENT_VARGS_HEADERS))																	{ http_client_header_pvp(c, args); }
		else if((v == HTTP_CLIENT_FORMDATA) && (flags & HTTP_CLIENT_VARGS_FORMDATA))															{ http_client_form_data_pvp(c, args); }
		else																																	{ v = 0; }
	}
}

// Data Handler (Socket Interface)
void http_client_on_recv(struct http_client *c, struct socket *s, uint8_t *data, uint16_t size)
{
	struct http_arg *a;
	uint16_t x;

	// Append Data to Response
	if((c->resbuf_p + size) > HTTP_CLIENT_RESBUF_SIZE)																							{ size = HTTP_CLIENT_RESBUF_SIZE - c->resbuf_p; }
	if(size)																																	{ memcpy(&(c->resbuf[c->resbuf_p]), data, size); c->resbuf_p = c->resbuf_p + size; }

	// Attempt to parse HTTP Response
	if(http_split_res((char *)c->resbuf, c->resbuf_p, &(c->res)))																				{ return; }

	// Check Content-Length
	a = http_client_res_head(c, HTTP_HEADER_CONTENTLENGTH);
	if(a)
	{
		x = atoi(http_arg_val(c, a));
		if(c->res.body_len < x)																													{ return; }
		if(c->res.body_len > x)																													{ c->res.body_len = x; }
	}

	// Mark Request Done
	c->state = HTTP_CLIENT_REQ_STATE_DONE;
}

// Disconnect Handler (Socket Interface)
void http_client_on_dcnt(struct http_client *c, struct socket *s)
{
	// Ensure Socket is Closed
	http_connector_drop(&(c->conn));

	// Check Request Done
	if(c->state == HTTP_CLIENT_REQ_STATE_DONE)																									{ return; }

	// Attempt to parse response
	http_client_on_recv(c, s, 0, 0);

	// Check Done
	if(c->state != HTTP_CLIENT_REQ_STATE_DONE)																									{ c->state = HTTP_CLIENT_REQ_STATE_FAIL; }
}

// Eloop Hook Handler
void http_client_ehook_handler(struct http_client *c)
{
	// Check Completed
	if(http_client_finished(c) == 0)																											{ return; }

	// Unhook
	eloop_unhook(&(c->ehook));

	// Notify
	if(c->on_complete)																															{ c->on_complete(c->on_complete_user, c); }
}
